%%This is a very basic article template.
%%There is just one section and two subsections.
\documentclass{article}

\usepackage{graphicx}
\usepackage{authblk}


\title{Eclipse-Resistant Network Overlays\\ for Fast Data Dissemination}
\date{Technical Report -- February 2021}

\renewcommand*{\Authsep}{ENA }
\renewcommand*{\Authand}{\hspace{1cm}}
\renewcommand*{\Authands}{TRIA }
%\renewcommand*{\Affilfont}{\normalsize}
%\renewcommand*{\Authfont}{\bfseries}    % make author names boldface    
\setlength{\affilsep}{2em}   % set the space between author and affiliation

\author{Evangelos Kolyvas\thanks{ekolyvas@aueb.gr}}
\author{Spyros Voulgaris\thanks{voulgaris@aueb.gr}}
\affil{Department of Informatics, Athens University of Economics and Business, Athens, Greece}
%\setcounter{Maxaffil}{0}
\renewcommand\Affilfont{\itshape\small}

\hyphenation{block-chain}

\begin{document}
\maketitle


\section{Introduction}

Blockchains were introduced just over a decade ago, bringing a revolution in a number of diverse fields including distributed computing, finance, healthcare, rights management, auctions, and in general all industries where \emph{provable trust}, \emph{accountability}, and \emph{transparency} play a key role.

At a conceptual level, a blockchain's task consists in accepting users' transactions, confirming their validity, and registering them in a globally agreed total order in an indisputable, immutable, and irrevocable way. Distributed consensus regarding the validity and the registration order of transactions stands at the core of blockchain systems' operation.

%In a nutshell, blockchains operate as follows.
A network of nodes, typically referred to as \emph{miners}, constitute a blockchain's distributed validation engine.
Miners are responsible for receiving users' transactions, distributing them among themselves, attesting to their validity, and registering the valid ones into ordered sets of transactions, known as \emph{blocks}.
In fact, achieving consensus regarding the global order of valid transactions can be considered as the core task of a blockchain system.


% More specifically, users submit transactions to the system by sending them to any one or more miners.
% Miners, subsequently, disseminate received transactions among themselves.
% Each miner tries to generate blocks as often as possible, as block generation brings monetary profits to the miner.
% Blockchain systems, however, impose mechanisms that regulate the rate at which blocks can be generated.
% This can be achieved by coupling block generation with a \emph{finite} resource.
% In the case of \emph{Proof of Work} (\emph{PoW}), the chosen finite resource is \emph{processing power}: miners are required to perform a number of calculations to be allowed to generate a block.
% In \emph{Proof of Stake}, block generation is coupled to a different finite resource, namely miners' \emph{stake}: that is, the amount of monetary assets a miner has or represents.

Maintaining the global order of transactions entails marking in an immutable manner the sequence in which blocks have been generated.
To achieve this, each block includes a cryptographic hash of its previous block, effectively linking to it and forming an append-only linked list known as the \emph{blockchain}.
Should two blocks happen to be generated in parallel (i.e., neither of the two miners being aware of the other block while generating their own), they both link to the same parent block, leading to a temporary ambiguity on what the official state of the chain is.
This situation is known as a \emph{fork}, in which two or more blocks reporting the same parent block are competing against each other.
Eventually only one of these blocks prevails, effectively rendering the other ones invalid, and wasting the effort put in generating them.
This has an adverse effect on the overall throughput of the blockchain, that is, the number of transactions processed per time unit.
To maintain a high transaction throughput and to prevent frequent forks, once a block has been generated it should be delivered to all other nodes the soonest possible, to let them work on the subsequent block in the chain.

%Thus, building block $B_{k+1}$ requires the knowledge of block $B_k$.

From the above it becomes evident that fast dissemination of newly created blocks to the entire network of miners is crucial for the performance but also the correctness of a blockchain system.
Most blockchains employ \emph{epidemic algorithms}, also known as \emph{gossiping algorithms}, for block dissemination.
This work explores epidemic dissemination protocols tailored to the distribution of new blocks in blockchain systems.




\section{Epidemic Dissemination}

There are two main epidemic dissemination paradigms: \emph{push-based} and \emph{pull-based} dissemination.

\subsection{Push-based vs. Pull-based Dissemination}
In push-based dissemination, when a node receives a message it has not seen before, it instantly forwards it to a number of other nodes, which in turn do the same.
Due to the reactive nature of this operation, new messages spread exponentially fast to a significant portion of the network.
The push paradigm is very efficient in the early stages of dissemination, when most nodes are still unaware of the new message, thus forwarding it to arbitrarily chosen nodes is likely to spread it further.
However, it suffers in later stages of dissemination, when most nodes have already received this message, therefore forwarding it arbitrarily will most likely deliver it to an already informed node, wasting network resources without benefit.
Even worse, as nodes have no control over \emph{who} should forward the new message to them, some nodes may never receive a given message simply because no other node happened to choose them in forwarding it.
To alleviate this shortcoming, push-based dissemination schemes often employ high levels of redundancy, so that the probability of any one node being left out diminishes, at the cost of very high network overhead.
Kermarrec et. al~\cite{kermarrec2003probabilistic} report that each node should forward a message to around 15 other nodes to probabilistically achieve complete dissemination, using network resources that are in the order of 15-fold higher than the optimal case of delivering a message to each node once.

In the alternative paradigm, pull-based dissemination, nodes periodically contact arbitrary other nodes to ask whether a new message is available, and to pull it from them in that case.
Due to its proactive nature and periodic polling, pull-based dissemination does not spread messages as fast as its push-based counterpart, most notably in the early stages of dissemination when most polls do not bring any news.
However, as each node is responsible for fetching new messages to itself, eventually every single node receives the message.
Moreover, periodic polling messages aside, the pull-based strategy is very network efficient, as every new message is delivered exactly \emph{once} per node.
Its moderate dissemination speed, though, makes it inapt for use as-is in blockchain systems.


\subsection{Subscription-based Dissemination}

In Cardano a third policy has been proposed for disseminating blocks, combining the best of both worlds. In this policy, which we refer to as \emph{subscription-based dissemination}, each node \emph{subscribes} to some other peers, which are then responsible to \emph{push} new messages to their subscribers as soon as they receive them.
The peers a node has subscribed to are referred to as that node's \emph{upstream peers}, while the peers subscribed to a given node are that node's \emph{downstream peers}.
The subscription-based policy combines the pull-based strategy's \emph{proactive} nature, in the sense that each node chooses its upstream peers in advance of any block's dissemination, with the push-based strategy's \emph{reactive} property, that is, that blocks are forwarded downstream as soon as they have been received by a node.






\section{System Model}
\label{sec:system}

We consider a network of nodes, known as \emph{stake pools}, in charge of producing blocks.
Time is split into \emph{slots}, one second long each.
Cardano's \emph{Ouroboros} consensus protocol, a \emph{Proof of Stake} protocol, probabilistically appoints nodes as \emph{slot leaders}, such that there is globally one leader elected every 20 slots, on average.
It should be emphasized that this is only the \emph{expected} rate at which leaders are elected; in practice leaders may be elected fewer than 20 slots apart, and even multiple leaders may happen to be elected on the same slot.
Only a slot leader is allowed to generate a block for the specific slot.
To protect nodes and the system from attacks on leaders of upcoming slots, and with the help of cryptographic Verifiable Random Functions (VRFs), no node can tell whether a certain node has been appointed leader for a given slot other than that node itself.
When a leader generates a block, it attaches a proof that it was indeed leader for that slot, and it disseminates the block along with the proof across the network.

As explained earlier, generating a block requires the knowledge of the previous block.
As such, once a new block has been generated it should be instantly sent to the next upcoming leader of one of the following slots.
However, as no node can know \emph{who} the leader of a future slot will be (other than itself), a new block should be delivered to \emph{all} nodes of the network in order to also make it readily available to the next upcoming leader.
This constitutes a serious challenge for the dissemination of blocks, as they should be urgently delivered to the entire network.

Block dissemination takes place as follows. When a node generates (being the current slot's leader) or receives a new block, it propagates it further by sending only the block's header to all its downstream peers.
This way a node may receive a given header by more than one of its upstream peers.
Upon receiving a header it has not already seen, a node asks the peer that sent it the header to also send it the block body.
Subsequently, the node locally validates the entire block, and once the check has succeeded it repeats the same procedure to disseminate the block further to its own downstream peers by forwarding them the header.






\section{Block Dissemination in Cardano}

\subsection{Solution Overview}

Fast dissemination at a global scale is a two-faceted endeavour.
First, a new block should be disseminated fast and exhaustively at a local scope, harnessing the low-latency links of geographically proximal locations and ensuring that every single nearby node receives the block through a fast, low-latency, local path.
Second, a dissemination algorithm should also encompass a global outlook, managing to spread the news fast to distant locations.

\begin{figure}[t]
  \includegraphics[width=\linewidth]{figs/world.pdf} 
  \caption{Dissemination overview: Blocks should be propagated to a few nearby and a few distant nodes.}
  \label{fig:world}
\end{figure}


Intuitively, a block generated in Amsterdam, should reach a node running in London fast, over local links, rather than over a long-distance path that first visits Sydney.
At the same time, the node in Sydney should also receive the block relatively fast, over a path that contains a direct shortcut from somewhere around Amsterdam to somewhere in Australia, rather than waiting for the block to slowly cross all of Europe and Asia by means of many local dissemination steps.

Figure~\ref{fig:world} illustrates an overview of the proposed strategy.
Blocks should be forwarded both across local, low-latency links, and distant, long-range ones. This brings up two questions: first, how to pick short-range and long-range links, and second, at what ratio.


\subsection{Proposed Solution}

The solution we propose is partially inspired by our prior work on P2P self-organization and epidemic dissemination.
More specifically, Hybrid Dissemination~\cite{hybrid_dissemination} and PolderCast~\cite{poldercast} propose decentralized algorithms for the dissemination of messages and pub/sub events, respectively, over a P2P network with remarkably high scalability and resilience to faults.
Our work on \cite{vicinity}~further explains the importance of blending determinism with randomness in large-scale self-organizing P2P systems, to maximize performance and network robustness.

In our dissemination protocol each node subscribes to a number of other nodes, which are picked out of two sets:
\begin{description}
\item[Close neighbors] These are neighbors exhibiting low network latency to the subscribing node. One can think of these neighbors as being geographically close to the subscriber, although our protocol is location-agnostic and is concerned exclusively with network latency.
\item[Random neighbors] These are nodes picked uniformly at random out of all participating nodes. The rationale behind this decision is that our protocol does not demand a multitude of nodes at each distant region, but rather just a few sporadic representatives. Therefore, if every node forwards a block across a few \emph{random} links, the block should quickly get widely dispersed across the world, albeit at a sparse density. Forwarding to close neighbors bridges the gap, turning a block's sparse distribution into a dense, exhaustive dissemination reaching every single participating node across the globe.
\end{description}

Note that we did also consider a combination of \emph{close} and \emph{distant} (instead of random) nodes, however we did not observe any improved behavior in the corresponding experiments. Moreover, choosing distant nodes as neighbors involves two risks.
First, by deterministically opting for highest-latency nodes as neighbors, nodes located in isolated areas (e.g., remote islands in the middle of an ocean), will result into a highly biased topology with a very high number of links towards these distant nodes.
Second, and even worse, as being a ``distant'' node can easily be emulated by merely delaying all communication, malicious nodes could easily take advantage of this to attract links from the entire network, something we want to prevent at all costs.
Thus, the policy of explicitly choosing distant nodes as neighbors has been ruled out since the early stages of our work.
It should also be noted that in contrast to being ``distant'', the property of being ``close'' to a given node cannot be emulated.

The process of discovering random and latency-wise close nodes is out of the scope of the current phase of this research.
Instead, we are investigating the type of overlay that performs best with respect to disseminating blocks across the entire network.
%That is, we are investigating the ratio at which random and close neighbors should be mixted to improve dissemination speed.

% To keep dissemination time low, an algorithm should leverage low-latency connections.
% New blocks should be disseminated to nearby nodes fast, 








\section{Experimental Setup}

\subsection{Simulation Environment}

We used the PeerNet~\cite{peernet} simulator to evaluate our proposed protocol.
PeerNet is an open-­source platform for the development, testing, and deployment of Peer-to-Peer applications.
It is a fork of the popular PeerSim simulator~\cite{peersim}\cite{montresor2009peersim}.
In \emph{simulation mode}, PeerNet allows the deterministic and reproducible simulation of protocols on a single machine, suitable for debugging and understanding the inner workings of decentralized protocols.
In \emph{network mode} it can execute the same protocols in a real-world distributed environment (e.g., the Planetlab or any cluster, grid, or IaaS cloud) without any changes in the imple­mentation, without even the need to recompile the code.


\subsection{Network Latency}
\label{sec:latency}

Network latency between pairs of nodes plays a crucial role in the performance, and hence the assessment, of the protocols under investigation.
As explained in Section~\ref{sec:system}, forwarding a block from a node A to another node B involves a three-way communication. First node A sends the header, then node B sends a request to also get the body, and finally node A sends the actual block body. That is, the time to propagate a block from A to B involves three times the latency between A and B, or, equivalently, one and a half times their round-trip time (RTT).

To model network latency between node pairs, we followed two options:

\begin{itemize}

\item As a first option we created an artificial latency model based on a torus-shaped space. We placed nodes uniformly at random on a torus, giving them random coordinates, and then we used the Euclidean distance between two nodes to determine the corresponding network latency between them. We set the maximum Euclidean distance on this torus to correspond to a latency of 250 msec, and all other pairs of nodes to have a latency proportional to their distance. This implies a maximum of 750 msec block propagation time (three times the latency).

\item As a second option we adopted the round-trip times (RTT) matrix of the King dataset~\cite{king}, a research study that measured the round-trip times among a large number of DNS servers around the world.
\end{itemize}

In contrast to the torus topology, the latencies provided by the King trace are not symmetric.
That is, the time it takes for a message to be sent from node A to node B is not necessarily equal to the time it takes for the message to be sent from node B to node A.
Besides that, the King dataset dates back to 2002, which is admittedly a very long period in Internet time, however it serves as a realistic set of worst-case latencies.
Figure~\ref{fig:latencies} shows the pairwise one-way latencies for all pairs of each topology.

\begin{figure}[h]
  \includegraphics{dat/latencies.pdf} 
  \caption{All-pair latencies for the Torus and the King dataset. Note that the block transfer time between two nodes is three times their one-way latency, due to the three-way communication involved and explained in Section~\ref{sec:latency}.}
  \label{fig:latencies}
\end{figure}


It should be noted that we also assumed a fixed \emph{processing delay} of 30 msec per hop in both topologies.
This delay captures the time it takes a node to process a block and validate its content.






\section{Evaluation}

We considered a network of 1000 nodes, and for each run we generated and disseminated 1000 distinct blocks.
Each block was generated at a node picked uniformly at random as slot leader for the corresponding slot.
For each block we recorded the time each node received it, to observe the progress of its dissemination.

Regarding the number of upstream peers per node (typically referred to as the node's \emph{valency}), we experimented with three values: 6, 10, and 20. Ten is the number currently suggested by IOHK, so trying also a lower and higher number gives a better view of the protocol's performance.

For each valency value, we considered all combinations of (latency-wise) \emph{close} and \emph{random} neighbors.
Each configuration combining $n$ close neighbors and $m$ random ones is code-named \texttt{C$n$\,R$m$}.
For instance, for valency 10, we considered the two edge cases of \texttt{C10} (all close neighbors) and \texttt{R10} (all random neighbors), as well as all mixed cases, i.e., \texttt{C1\,R9}, \texttt{C2\,R8}, \ldots,  \texttt{C9\,R1}.


\subsection{Torus Topology}


\begin{figure}
  \includegraphics{dat/torus_n1000_d06.pdf} \\ 
  \includegraphics{dat/torus_n1000_d10.pdf} \\ 
  \includegraphics{dat/torus_n1000_d20.pdf} 
  \caption{Dissemination performance for the torus topology. \texttt{C$x$\,R$y$} refers to the configuration with $x$ close and $y$ random upstream peers per node.}
  \label{fig:torus_times}
\end{figure}

Figure~\ref{fig:torus_times} presents the progress of dissemination for the torus scenario, considering a valency of 6 (top), 10 (middle), and 20 (bottom).
The progress is shown by plotting the number of nodes that have not yet received the new block, as a function of the time elapsed since that block's generation (in milliseconds).

Configurations involving mostly random upstream peers are color-coded blue at different shades: dark when all neighbors are random, and paler as close nodes start mingling in.
Likewise, configurations involving mostly close nodes are color-coded yellow: the all-close scenario being plotted in dark yellow, and the color fading away as random peers start mingling in.
Finally, the middle case, where upstream peers are equally split between random and close ones, is plotted in a thicker, red line to make it visually distinguishable.


A first observation is that the two extreme scenarios (all-random and all-close neighbors) demonstrate the worst performance.
This was expected behavior, in accordance to the findings in~\cite{vicinity}.
When only random neighbors are selected (dark blue lines), a node may end up receiving a block through a long distance path, even if its ``next door neighbor'' has already received it much earlier.
When only close neighbors are selected (dark yellow lines), dissemination is relatively faster than the former extreme case, as once the block has approached a node's vicinity that node is very likely to receive it fast, however it runs a severe risk: As nodes focus exclusively on their local neighborhoods without any random links, there is a risk of some nodes forming a disjoint cluster and having no communication to the rest of the world. In our experiments this happened in the experiments of valency 6 (top plot), where the dark yellow line can be seen ``stuck'' close to 1000, indicating that the network is seggregated into many small clusters and dissemination is not possible.

Performance instantly gets significantly better when mixing some randomness into the all-close scenario, or when adding some close nodes to the all-random scenario. The scenario where neighbors have been split in half between close and random nodes (red line), performs optimally or practically almost optimally in all valencies we considered.


\subsection{King-based Topology}

\begin{figure}
  \includegraphics{dat/king_n1000_d06.pdf} \\ 
  \includegraphics{dat/king_n1000_d10.pdf} \\ 
  \includegraphics{dat/king_n1000_d20.pdf} 
  \caption{Dissemination performance for the King dataset topology.}
  \label{fig:king_times}
\end{figure}


Figure~\ref{fig:king_times} illustrates the dissemination progress for the network topology where latencies are based on the King dataset.
Similarly to the experiments on the torus topology, we considered a valency of 6, 10, and 20, and we use the same color coding.

Our initial observation is that we see the same pattern as for the torus topology: the two extremes, namely, the all-random and all-close scenarios, demonstrate the worst performance of all.
In fact the all-close scenario fails to disseminate blocks for all considered valency values.
This indicates that the king topology, referring to real-world measurements, contains nodes that are seggregated in different geographic locations, so when they pick strictly the closest neighbors the whole overlay falls apart into multiple disjoint components.

Splitting neighbors equally between random and close peers, appears to be the most prominent strategy again, as it is always optimal or very close to optimal, particularly for the more realistic valency of 10.

Another observation is that dissemination seems to complete a bit faster in the king topology compared to the torus.
This reflects the lower latencies exhibited by the vast majority of pairs in the former in comparison to the latter, as we have seen in Figure~\ref{fig:latencies}.


\subsection{Effect of Node Valency}

\begin{figure}
  \includegraphics{dat/torus_n1000.pdf} \\ 
  \includegraphics{dat/king_n1000.pdf} 
  \caption{Effect of node valency on dissemination performance.}
  \label{fig:degree_range}
\end{figure}

To assess the effect of node valency on dissemination speed, we compare the dissemination progress for a range of valencies, both for the torus and king topologies.
We fix these experiments to an equal split of the available neighbors between random and close peers, as this configuration was shown to be the most prominent one.

Figure~\ref{fig:degree_range} shows the respective plots for node valency ranging from 6 to 20.
As expected and as we have already seen, increasing the number of upstream peers per node increases dissemination speed.
However, we see that the improvement rate slows down as we move to higher valency values.
Notably, the performance boost achieved by adding just two extra neighbors to the \texttt{C3\,R3} configuration to move to \texttt{C4\,R4}, is far greater than that achieved by adding six neighbors to move from \texttt{C7\,R7} to \texttt{C10\,R10}.





\section{Conclusions and Next Steps}

In this technical report we address the problem of block dissemination in blockchain systems, focusing on the Cardano blockchain.
The specific design details of the Cardano blockchain require that new blocks reach the entire network within very few seconds.
This renders the dissemination problem particularly challenging.

We proposed a dissemination protocol that propagates new blocks over links to two types of neighbors: (i) some of the latency-wise closest peers to a node, and (ii) a few nodes picked uniformly at random out of all participants.
We subsequently explored the parameter space to see what combination of close and random neighbors exhibits better dissemination performance. 

We chose two different topologies to run our experiments in order to compare our proposed protocol in graphs with different characteristics.
Despite their diversity, the policy that shows to be optimal is quite the same in both cases: a balance between close and random neighbors.

Please take a note that the time shown in the figures is just to compare between the different neighbor-picking-policies, not a real Cardano metric.
In the next steps of our research will try to create a Cardano-tailor-made topology in order to get more accurate time results.

In the following steps of this research we plan to move along these directions:
\begin{itemize}
\item Use real-world Cardano stake-pool data to compile a dataset of realistic and up-to-date matrix of node latencies.

\item Add and experiment with a third metric for selecting neighbors, besides picking close and random ones, namely picking the ones that tend to deliver block headers faster, based on a local scoring mechanism.

\item Incorporate mechanisms also to \emph{build} the target overlay, in addition to \emph{using} it for dissemination.

\item Focus on mechanisms that will shield the ledger from eclipse attacks that may affect its dissemination correctness.
\end{itemize}

\bibliographystyle{plain}
\bibliography{refs}

\end{document}
