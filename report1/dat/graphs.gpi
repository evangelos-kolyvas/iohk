#set term postscript eps enhanced 13 color
#set output '| epstopdf --filter > torus_d10.pdf'

set term pdfcairo size 5,2.5
set term pdfcairo size 4.5,2.25

KMULT=1.5

#set style line 1  lt 1 lc rgb '#A90E09' lw 3
#set style line 2  lt 1 lc rgb '#B3350C' lw 3
#set style line 3  lt 1 lc rgb '#BC4A0F' lw 3
#set style line 4  lt 1 lc rgb '#C45911' lw 3
#set style line 5  lt 1 lc rgb '#CC6614' lw 3
#set style line 6  lt 1 lc rgb '#D47116' lw 5
#set style line 7  lt 1 lc rgb '#DD8C19' lw 3
#set style line 8  lt 1 lc rgb '#E6A11B' lw 3
#set style line 9  lt 1 lc rgb '#EEB41E' lw 3
#set style line 10 lt 1 lc rgb '#F6C420' lw 3
#set style line 11 lt 1 lc rgb '#FED322' lw 3

#set style line 1  lt 1 lc rgb '#C7162B' lw 3
#set style line 2  lt 1 lc rgb '#B64326' lw 3
#set style line 3  lt 1 lc rgb '#A15B20' lw 3
#set style line 4  lt 1 lc rgb '#8A6D19' lw 3
#set style line 5  lt 1 lc rgb '#6B7C0F' lw 3
#set style line 6  lt 1 lc rgb '#398800' lw 3
#set style line 7  lt 1 lc rgb '#849B0B' lw 3
#set style line 8  lt 1 lc rgb '#AEAB13' lw 3
#set style line 9  lt 1 lc rgb '#CDBA19' lw 3
#set style line 10 lt 1 lc rgb '#E7C71E' lw 3
#set style line 11 lt 1 lc rgb '#FED322' lw 3

#set style line 1  lt 1 lc rgb '#2098DBF1' lw 3
#set style line 2  lt 1 lc rgb '#20A3C6DB' lw 3
#set style line 3  lt 1 lc rgb '#20ADAFC2' lw 3
#set style line 4  lt 1 lc rgb '#20B692A3' lw 3
#set style line 5  lt 1 lc rgb '#20BF6B7A' lw 3
#set style line 6  lt 1 lc rgb '#00C7162B' lw 5
#set style line 7  lt 1 lc rgb '#20D46E3B' lw 3
#set style line 8  lt 1 lc rgb '#20E09647' lw 3
#set style line 9  lt 1 lc rgb '#20EBB351' lw 3
#set style line 10 lt 1 lc rgb '#20F5CB5A' lw 3
#set style line 11 lt 1 lc rgb '#20FFE062' lw 3

set style line 1  lt 1 lc rgb '#003882FF' lw 3
set style line 2  lt 1 lc rgb '#403882FF' lw 3
set style line 3  lt 1 lc rgb '#803882FF' lw 3
set style line 4  lt 1 lc rgb '#A03882FF' lw 3
set style line 5  lt 1 lc rgb '#D03882FF' lw 3
set style line 6  lt 1 lc rgb '#00C7162B' lw 5
set style line 7  lt 1 lc rgb '#D0FED322' lw 3
set style line 8  lt 1 lc rgb '#A0FED322' lw 3
set style line 9  lt 1 lc rgb '#80FED322' lw 3
set style line 10 lt 1 lc rgb '#40FED322' lw 3
set style line 11 lt 1 lc rgb '#00FED322' lw 3
set style line 12 lt 1 dt 4 lc rgb '#00000000' lw 1


set ytics nomirror
set grid ytics
set grid xtics

#set lmargin 8
#set rmargin 0

set nologscale x
set logscale y
#set format y "10^{%L}"

set autoscale x
set xrange [0:2100]
set yrange [1:*]

set xlabel "time (msec)"
#set ylabel "uninformed nodes (%)"
set ylabel "not yet informed nodes"
set key bottom left reverse Left font ",10"


set title 'Torus topology, 1000 nodes, 6 neighbors per node'
set output 'torus_n1000_d06.pdf'
plot 'time_n1000_d06c00r06_torus.dat' using 1:2 with lines ls 1 title 'R6 (random only)', \
     'time_n1000_d06c01r05_torus.dat' using 1:2 with lines ls 2 title 'C1 R5', \
     'time_n1000_d06c02r04_torus.dat' using 1:2 with lines ls 3 title 'C2 R4', \
     'time_n1000_d06c03r03_torus.dat' using 1:2 with lines ls 6 title 'C3 R3', \
     'time_n1000_d06c04r02_torus.dat' using 1:2 with lines ls 9 title 'C4 R2', \
     'time_n1000_d06c05r01_torus.dat' using 1:2 with lines ls 10 title 'C5 R1', \
     'time_n1000_d06c06r00_torus.dat' using 1:2 with lines ls 11 title 'C6 (close only)'

set title 'Torus topology, 1000 nodes, 10 neighbors per node'
set output 'torus_n1000_d10.pdf'
plot 'time_n1000_d10c00r10_torus.dat' using 1:2 with lines ls 1 title 'R10 (random only)', \
     'time_n1000_d10c01r09_torus.dat' using 1:2 with lines ls 2 title 'C1 R9', \
     'time_n1000_d10c02r08_torus.dat' using 1:2 with lines ls 3 title 'C2 R8', \
     'time_n1000_d10c03r07_torus.dat' using 1:2 with lines ls 4 title 'C3 R7', \
     'time_n1000_d10c04r06_torus.dat' using 1:2 with lines ls 5 title 'C4 R6', \
     'time_n1000_d10c05r05_torus.dat' using 1:2 with lines ls 6 title 'C5 R5', \
     'time_n1000_d10c06r04_torus.dat' using 1:2 with lines ls 7 title 'C6 R4', \
     'time_n1000_d10c07r03_torus.dat' using 1:2 with lines ls 8 title 'C7 R3', \
     'time_n1000_d10c08r02_torus.dat' using 1:2 with lines ls 9 title 'C8 R2', \
     'time_n1000_d10c09r01_torus.dat' using 1:2 with lines ls 10 title 'C9 R1', \
     'time_n1000_d10c10r00_torus.dat' using 1:2 with lines ls 11 title 'C10 (close only)'

set title 'Torus topology, 1000 nodes, 20 neighbors per node'
set output 'torus_n1000_d20.pdf'
plot 'time_n1000_d20c00r20_torus.dat' using 1:2 with lines ls 1 title 'R20 (random only)', \
     'time_n1000_d20c02r18_torus.dat' using 1:2 with lines ls 2 title 'C2 R18', \
     'time_n1000_d20c04r16_torus.dat' using 1:2 with lines ls 3 title 'C4 R16', \
     'time_n1000_d20c06r14_torus.dat' using 1:2 with lines ls 4 title 'C6 R14', \
     'time_n1000_d20c08r12_torus.dat' using 1:2 with lines ls 5 title 'C8 R12', \
     'time_n1000_d20c10r10_torus.dat' using 1:2 with lines ls 6 title 'C10 R10', \
     'time_n1000_d20c12r08_torus.dat' using 1:2 with lines ls 7 title 'C12 R8', \
     'time_n1000_d20c14r06_torus.dat' using 1:2 with lines ls 8 title 'C14 R6', \
     'time_n1000_d20c16r04_torus.dat' using 1:2 with lines ls 9 title 'C16 R4', \
     'time_n1000_d20c18r02_torus.dat' using 1:2 with lines ls 10 title 'C18 R2', \
     'time_n1000_d20c20r00_torus.dat' using 1:2 with lines ls 11 title 'C20 (close only)'

set key top right noreverse Right
set title 'Torus topology, 1000 nodes'
set output 'torus_n1000.pdf'
plot 'time_n1000_d06c03r03_torus.dat' using 1:2 with lines ls 1 title 'C3 R3', \
     'time_n1000_d08c04r04_torus.dat' using 1:2 with lines ls 2 title 'C4 R4', \
     'time_n1000_d10c05r05_torus.dat' using 1:2 with lines ls 3 title 'C5 R5', \
     'time_n1000_d14c07r07_torus.dat' using 1:2 with lines ls 4 title 'C7 R7', \
     'time_n1000_d20c10r10_torus.dat' using 1:2 with lines ls 5 title 'C10 R10'




set key top right noreverse Right


set title 'King topology, 1000 nodes, 6 neighbors per node'
set output 'king_n1000_d06.pdf'
plot 'time_n1000_d06c00r06_king.dat' using ($1*KMULT):2 with lines ls 1 title 'R6 (random only)', \
     'time_n1000_d06c01r05_king.dat' using ($1*KMULT):2 with lines ls 2 title 'C1 R5', \
     'time_n1000_d06c02r04_king.dat' using ($1*KMULT):2 with lines ls 3 title 'C2 R4', \
     'time_n1000_d06c03r03_king.dat' using ($1*KMULT):2 with lines ls 6 title 'C3 R3', \
     'time_n1000_d06c04r02_king.dat' using ($1*KMULT):2 with lines ls 9 title 'C4 R2', \
     'time_n1000_d06c05r01_king.dat' using ($1*KMULT):2 with lines ls 10 title 'C5 R1', \
     'time_n1000_d06c06r00_king.dat' using ($1*KMULT):2 with lines ls 11 title 'C6 (close only)'


set title 'King topology, 1000 nodes, 10 neighbors per node'
set output 'king_n1000_d10.pdf'
plot 'time_n1000_d10c00r10_king.dat' using ($1*KMULT):2 with lines ls 1 title 'R10 (random only)', \
     'time_n1000_d10c01r09_king.dat' using ($1*KMULT):2 with lines ls 2 title 'C1 R9', \
     'time_n1000_d10c02r08_king.dat' using ($1*KMULT):2 with lines ls 3 title 'C2 R8', \
     'time_n1000_d10c03r07_king.dat' using ($1*KMULT):2 with lines ls 4 title 'C3 R7', \
     'time_n1000_d10c04r06_king.dat' using ($1*KMULT):2 with lines ls 5 title 'C4 R6', \
     'time_n1000_d10c05r05_king.dat' using ($1*KMULT):2 with lines ls 6 title 'C5 R5', \
     'time_n1000_d10c06r04_king.dat' using ($1*KMULT):2 with lines ls 7 title 'C6 R4', \
     'time_n1000_d10c07r03_king.dat' using ($1*KMULT):2 with lines ls 8 title 'C7 R3', \
     'time_n1000_d10c08r02_king.dat' using ($1*KMULT):2 with lines ls 9 title 'C8 R2', \
     'time_n1000_d10c09r01_king.dat' using ($1*KMULT):2 with lines ls 10 title 'C9 R1', \
     'time_n1000_d10c10r00_king.dat' using ($1*KMULT):2 with lines ls 11 title 'C10 (close only)'

set title 'King topology, 1000 nodes, 20 neighbors per node'
set output 'king_n1000_d20.pdf'
plot 'time_n1000_d20c00r20_king.dat' using ($1*KMULT):2 with lines ls 1 title 'R20 (random only)', \
     'time_n1000_d20c02r18_king.dat' using ($1*KMULT):2 with lines ls 2 title 'C2 R18', \
     'time_n1000_d20c04r16_king.dat' using ($1*KMULT):2 with lines ls 3 title 'C4 R16', \
     'time_n1000_d20c06r14_king.dat' using ($1*KMULT):2 with lines ls 4 title 'C6 R14', \
     'time_n1000_d20c08r12_king.dat' using ($1*KMULT):2 with lines ls 5 title 'C8 R12', \
     'time_n1000_d20c10r10_king.dat' using ($1*KMULT):2 with lines ls 6 title 'C10 R10', \
     'time_n1000_d20c12r08_king.dat' using ($1*KMULT):2 with lines ls 7 title 'C12 R8', \
     'time_n1000_d20c14r06_king.dat' using ($1*KMULT):2 with lines ls 8 title 'C14 R6', \
     'time_n1000_d20c16r04_king.dat' using ($1*KMULT):2 with lines ls 9 title 'C16 R4', \
     'time_n1000_d20c18r02_king.dat' using ($1*KMULT):2 with lines ls 10 title 'C18 R2', \
     'time_n1000_d20c20r00_king.dat' using ($1*KMULT):2 with lines ls 11 title 'C20 (close only)'

set title 'King topology, 1000 nodes'
set output 'king_n1000.pdf'
plot 'time_n1000_d06c03r03_king.dat' using ($1*KMULT):2 with lines ls 1 title 'C3 R3', \
     'time_n1000_d08c04r04_king.dat' using ($1*KMULT):2 with lines ls 2 title 'C4 R4', \
     'time_n1000_d10c05r05_king.dat' using ($1*KMULT):2 with lines ls 3 title 'C5 R5', \
     'time_n1000_d14c07r07_king.dat' using ($1*KMULT):2 with lines ls 4 title 'C7 R7', \
     'time_n1000_d20c10r10_king.dat' using ($1*KMULT):2 with lines ls 5 title 'C10 R10'


set autoscale x
set autoscale y
set title 'Torus topology, 1000 nodes, 10 neighbors per node'
set output 'torus_deg_n1000_d10.pdf'
plot 'degree_n1000_d10c00r10_torus.dat' using 1:2 with histeps ls 1 title 'R10 (random only)', \
     'degree_n1000_d10c01r09_torus.dat' using 1:2 with histeps ls 2 title 'C1 R9', \
     'degree_n1000_d10c02r08_torus.dat' using 1:2 with histeps ls 3 title 'C2 R8', \
     'degree_n1000_d10c03r07_torus.dat' using 1:2 with histeps ls 4 title 'C3 R7', \
     'degree_n1000_d10c04r06_torus.dat' using 1:2 with histeps ls 5 title 'C4 R6', \
     'degree_n1000_d10c05r05_torus.dat' using 1:2 with histeps ls 6 title 'C5 R5', \
     'degree_n1000_d10c06r04_torus.dat' using 1:2 with histeps ls 7 title 'C6 R4', \
     'degree_n1000_d10c07r03_torus.dat' using 1:2 with histeps ls 8 title 'C7 R3', \
     'degree_n1000_d10c08r02_torus.dat' using 1:2 with histeps ls 9 title 'C8 R2', \
     'degree_n1000_d10c09r01_torus.dat' using 1:2 with histeps ls 10 title 'C9 R1', \
     'degree_n1000_d10c10r00_torus.dat' using 1:2 with histeps ls 11 title 'C10 (close only)'
